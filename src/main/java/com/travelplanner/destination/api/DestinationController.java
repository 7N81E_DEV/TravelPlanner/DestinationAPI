package com.travelplanner.destination.api;

import com.travelplanner.destination.model.Destination;
import com.travelplanner.destination.repository.DestinationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping(value = "1.0")
public class DestinationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DestinationRepository.class);
    private final DestinationRepository destinationRepository;

    public DestinationController(DestinationRepository destinationRepository) {
        this.destinationRepository = destinationRepository;
    }

    @RequestMapping(value = "/destination", method = RequestMethod.GET)
    public List<Destination> getAllUsers() {
        LOGGER.info("Getting all destinations.");
        return destinationRepository.findAll();
    }

    @RequestMapping(value = "/destination/{destinationId}", method = RequestMethod.GET)
    public Optional<Destination> getUser(@PathVariable String destinationId) {
        LOGGER.info("Getting destination with ID: {}.", destinationId);
        return destinationRepository.findById(destinationId);
    }

    @RequestMapping(value = "/destination/create", method = RequestMethod.POST)
    public Destination addNewUsers(@RequestBody Destination destination) {
        LOGGER.info("Saving destination.");
        return destinationRepository.save(destination);
    }

}
