package com.travelplanner.destination.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public final class Destination {

    @JsonProperty("destinationId")
    @Id
    private String destinationId;
    @JsonProperty("destinationName")
    private String destinationName;
    @JsonProperty("address1")
    private String address1;
    @JsonProperty("address2")
    private long address2;
    @JsonProperty("city")
    private String city;
    @JsonProperty("coordinates")
    private Coordinates coordinates;
    @JsonProperty("description")
    private String description;

    public String getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(String destinationId) {
        this.destinationId = destinationId;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public long getAddress2() {
        return address2;
    }

    public void setAddress2(long address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public static final class Coordinates {
        @JsonProperty("latitude")
        private String latitude;
        @JsonProperty("longitude")
        private String longitude;

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }
    }
}
